/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.flomedia.ocb;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Matt
 */
@WebServlet(name = "DoLogin", urlPatterns = {"/dologin"})
public class DoLogin extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("login");
        String pass = request.getParameter("pass");

        if (user.equals("test") && pass.equals("test")) {
            HttpSession session = request.getSession();
            session.setAttribute("user", new StringWrapper(user));
            Cookie c = new Cookie("user", user);
            c.setMaxAge(30 * 60);
            response.addCookie(c);
            //setting session to expiry in 30 mins
            String encodedURL = response.encodeRedirectURL("login");
            response.sendRedirect(encodedURL);

        } else {
            PrintWriter out = response.getWriter();
            out.println("<font color=red>Either user name or password is wrong.</font>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
